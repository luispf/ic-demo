package models

import (
	"testing"
)

func TestAddNotes(t *testing.T) {
	nota := &Nota{
		Cliente: "ic-demo",
	}
	partida := &Partida{
		Id:1,
		Description:"integracion continua",
		Cantidad: 2,
		Precio: 2.34,
	}

	nota.AddPartida(partida)
	partidaBuscar := nota.GetPartida(partida.Id)
	if partidaBuscar.Id != partida.Id {
		t.Error("la Partida no coincide!")
	}
}
