package models

type Nota struct{
	Cliente string
	Partidas []*Partida
	Total float64
}
type Partida struct {
	Id int
	Description string
	Cantidad float64
	Precio float64
	Importe float64
}
//AddPartida persiste una partida en nuestra nota
func (n *Nota) AddPartida(p *Partida) {
	p.Calcular()
	n.Partidas = append(n.Partidas,p)
	n.Calcular()
}
//GetPartida retorna la partida buscada
func (n *Nota) GetPartida(id int) *Partida {
	finded := &Partida{}
	for _,p := range n.Partidas {
		if p.Id == id {
			finded = p
			break
		}
	}
	return finded
}

//Calcular retorna monto total nota
func (n *Nota) Calcular() float64 {
	n.Total = 0
	for _,p := range n.Partidas {
		n.Total += p.Importe
		//mas calculos aqui
	}
	return n.Total
}
//Calcular retorna los montos de una partida
func (p *Partida) Calcular() float64 {
	p.Importe = 0
	p.Importe = p.Cantidad * p.Precio
	return p.Importe
}

